---
layout: homepage
title: School on Verification of Mobile and Autonomous Robots and Workshop
---

Call for Participation 
----------------------
York, 1-4 December, 2015<br>
<a href="https://www.cs.york.ac.uk/circus/RoboCalc-event">www.cs.york.ac.uk/circus/RoboCalc-event</a>

We invite applications from researchers to attend a series of short courses
ministered by the some of the leaders on verification of mobile and
autonomous robots in the UK.  Partial funding is available to support
participation of research students. The event is funded by the EPSRC via the
University of York and via EPSRC's Network on the Verification and Validation
of Autonomous Systems.

Courses
-------

* Towards Verification of Domestic Autonomous Robot Assistants <br>
  <a href="http://cgi.csc.liv.ac.uk/~clare/">Clare Dixon</a>, University of Liverpool 

* Techniques for Practical Verification in Robotics <br>
  <a href="http://www.cs.bris.ac.uk/~eder/">Kerstin Eder</a> (<a href="http://www.brl.ac.uk/vv">also</a>), University of Bristol

* Verifiable Autonomy - how can you trust your robots? <br>
  <a href="http://cgi.csc.liv.ac.uk/~michael/">Michael Fisher</a>, University of Liverpool

* From robot swarms to ethical robots: the challenges of verification and validation <br>
  <a href="http://www.cems.uwe.ac.uk/~a-winfield/">Alan Winfield</a>, Bristol Robotics Lab, University of the West of England, Bristol

* Why is verification and validation so crucial for the success of advanced robotics? <br>
  <a href="http://people.uwe.ac.uk/Pages/person.aspx?accountname=campus\ag-pipe">Anthony Pipe</a>, University of West England

* Selected Topics in Robotic Verification <br>
  <a href="https://www.shef.ac.uk/acse/staff/smv">Sandor Veres</a>, University of Sheffield

We will also have a panel that will discuss the state of the art
and the open problems in the area. More information about the
courses, the lecturers and local arrangements can be found at
www.cs.york.ac.uk/circus/robocal-event.

Application
-----------

To apply, please, send a message to Bob.French@york.ac.uk stating

* your name,
* affiliation, and
* position

by *31 October, 2015*.

Funding for PhD students
------------------------

The EPSRC Network on the Verification and Validation of Autonomous
Systems will provide limited travel and subsistence support for PhD
student participation in this Winter School. The procedure to apply
for PhD student travel and subsistence funding of up to £300 is
available <a href="{{ site.baseurl }}/funding-application">here</a>.