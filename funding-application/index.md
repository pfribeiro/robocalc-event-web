---
layout: homepage
title: Funding for PhD Students
breadcrumb: Funding for PhD Students
---
The EPSRC Network on the Verification and Validation of Autonomous Systems will provide limited travel support for PhD student participation in this Winter School.

To apply for PhD student travel/subsistence funding of up to £300, please email Bob French at Bob.French@york.ac.uk by 25th October 2015 with:

* confirmation that:
	* the applicant is a PhD student working in the area of autonomy, robotics or verification
	* how much funding is required (if not the full £300)
	* how the PhD is funded

* confirmation by the applicant's PhD supervisor that:
	* the applicant is a PhD student
	* attendance at the Winter School will be valuable
	* where the PhD funding is from

If allocated travel/subsistence funding you will be notified in early November 2015. You will be reimbursed following the event on production of the relevant receipts. Preference will be given to EPSRC funded PhD students.