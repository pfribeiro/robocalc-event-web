---
layout: homepage
title: Lecturers
breadcrumb: Lecturers
---
<ul class="list-unstyled">
{% for lecturers in site.data.bio %}
{% assign lecturer = lecturers[1] %}
<li>
		<div style="float: left; width: 80px; height: 80px; overflow: hidden; border-radius: 50%; margin-right: 0.5em; margin-bottom: 0.5em;">
        	{% if lecturer.pic_width %}
                <img width="{{lecturer.pic_width}}" style="margin-right: 1em; margin-top: {{ lecturer.pic_margin_top }}; margin-left: {{ lecturer.pic_margin_left }}; overflow: hidden;" src="{{lecturer.pic}}">
            {% else %}
                <img width="80" style="margin-right: 1em; margin-top: {{ lecturer.pic_margin_top }}; margin-left: {{ lecturer.pic_margin_left }}; overflow: hidden;" src="{{lecturer.pic}}">
            {% endif %}</div>
        	
        		<h2><a href="{{lecturer.url}}" class="link">{{ lecturer.name }}</a></h2>
        		<em>{{ lecturer.uni }}</em><br><br>
        	
	<div style="clear: both;">
		
    	{{ lecturer.bio | markdownify }}
    </div>
</li>
{% endfor %}
</ul>