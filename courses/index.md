---
layout: homepage
title: Courses
breadcrumb: Courses
---
<ul class="list-unstyled">
{% for course in site.courses %}
{% assign lecturer = site.data.bio[course.lecturer] %}
<li><h3><a href="{{course.url}}">{{ course.title }}</a></h3>{{ lecturer.name }}, <em>{{ lecturer.uni }}</em><br><br>
{{ course.abstract | markdownify }}
</li>
{% endfor %}
</ul>