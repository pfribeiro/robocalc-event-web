---
layout: course
title: Why is verification and validation so crucial for the success of advanced robotics?
lecturer: anthony-pipe
breadcrumb: Why is verification and validation so crucial for the success of advanced robotics?
abstract: 
 It is clear that many of the application domains for advanced robotics currently
 targeted by the robotics research community will be those where the barrier 
 between them and the, often highly unstructured, rest of the world will be torn 
 down. Development of a certification process for such devices will be imperative. 
 It follows that such a critical part of this certification process will be a safety-case 
 based argument and that, in turn, this will have to be backed up by verification 
 and validation of the correct behaviour of such robots acting in these settings. In 
 this session, we will discuss these issues in some depth, considering not only the 
 technology challenges, but also in the context of critical issues for acceptability, 
 as well as new financial, legal, regulatory and insurance structures that may be 
 required. The discussion will be set in the context of an upcoming revolution in, 
 what this presenter would consider to be robotics; the emergence of 
 autonomous vehicles on our roads.
---

