---
layout: course
title: Towards Verification of Domestic Autonomous Robot Assistants
lecturer: clare-dixon
abstract: Robotic assistants are being designed to help, or work with humans, in a variety of situations from assistance within domestic situations, through medical care, to within industrial settings. Whilst robots have been used in industry for some time they are often limited in terms of their range of movement or range of tasks. A new generation of robotic assistants have more freedom to move, and are able to autonomously make decisions and decide between alternatives. For people to adopt such robots they will have to be shown to be both safe and trustworthy. In these lectures we discuss our experiences in applying formal verification to a robotic assistant located in a typical domestic environment. We focus on the Care-O-bot located in the Robot House at the University of Hertfordshire. This work was carried out as part of the EPSRC funded Trustworthy Robotic Systems Project between the University of Liverpool, the University of Hertfordshire and Bristol Robotics Lab.
---
