---
layout: course
title: "From robot swarms to ethical robots: the challenges of verification and validation"
lecturer: alan-winfield
breadcrumb: "From robot swarms to ethical robots: the challenges of verification and validation"
abstract: |
 This lecture will be presented in two parts

 ### Part 1 From robot swarms...
 Swarm robotics is a new approach to multi-robot systems in which control is completely 
 decentralised and distributed. Inspired by the self-organising collective behaviours of social 
 insects each robot has a simple set of behaviours with local sensing and communications. The 
 overall desired system behaviours emerge from the local interactions between robots, and 
 between robots and their environment. The promise of swarm robotics is that it offers the 
 potential of highly adaptable, scalable and resilient systems, able to tolerate the failure of many 
 individual robots without loss of overall system (i.e. swarm) functionality. Significant engineering 
 challenges, however, are firstly how to design the emergent, self-organising properties of the 
 swarm, and secondly how to assure – and ideally verify – safe and dependable operation.
 Here I will briefly (1) introduce swarm robotics, illustrating the state-of-the-art with respect to 
 current research, both within the Bristol Robotics Lab and elsewhere. (2) Using adaptive swarm 
 foraging as a case study, we will address the engineering challenges of mathematical modelling 
 and optimization. Then (3) we will analyse the robustness and scalability of a swarm robotic 
 system by developing a model of its reliability. 

 ### Part 2 ...to ethical robots
 If robots are to be trusted, especially when interacting with humans, then they will need to be 
 more than just safe. In this talk I will outline the potential of robots capable of modelling and 
 therefore predicting the consequences of both their own actions and the actions of other 
 dynamic actors in their environment. I will show that with the addition of an ‘ethical’ action 
 selection mechanism a robot can sometimes choose actions that compromise its own safety in 
 order to prevent a second robot from coming to harm. An implementation with both e-puck and 
 NAO mobile robots provides a proof of principle by showing that a robot can, in real time, model 
 and act upon the consequences of both its own and another robot’s actions. I argue that this 
 work moves us towards robots that are ethical, as well as safe.
 I will conclude by discussing the challenges of verification and validation of an ethical robot.
---
