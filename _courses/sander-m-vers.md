---
layout: course
title: Selected Topics in Verification of Robots
lecturer: sandor-m-veres
breadcrumb: Selected Topics in Verification of Robots
abstract: |
 02.00-02.45  Introduction to model checking of engineering systems

 03.00-03.45  Verification of Control Systems on UAS 

 03.45-04.00  Coffee break

 04.00-04.45  Verification of runtime reasoning on robots

 05.00-05.45  Pursuit evasion strategies by model checking
---


